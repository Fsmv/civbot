package main

import (
	"database/sql"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	_ "github.com/mattn/go-sqlite3"
)

var (
	dbfile           = flag.String("dbfile", "civgames.sqlite", "The SQLite DB file to use for the game data")
	token            = flag.String("discord_token", os.Getenv("DISCORD_TOKEN"), "Discord Bot Token")
	defaultChannelID = flag.String("default_discord_channel_id", os.Getenv("DISCORD_CHANNEL_ID"), "The Discord Channel ID to send messages to when a game was not added to the database.")

	nameTakenError = errors.New("Sorry, that civ name is already taken.")
)

const (
	commandPrefix = "/"
)

var (
	db              *sql.DB
	bot             *discordgo.Session
	gameTurnCounter map[string]int // used only for duplicate notification rejection

)

// WebhookMessage is the data received from a webhook call
type WebhookMessage struct {
	GameName   string `json:"value1"`
	PlayerName string `json:"value2"`
	TurnNumber string `json:"value3"`
}

func init() {
	flag.Parse()
	gameTurnCounter = make(map[string]int)
}

func main() {
	var err error
	db, err = sql.Open("sqlite3", *dbfile)
	if err != nil {
		log.Fatal("Failed to open database:", err)
	}
	defer db.Close()
	if err := createTables(); err != nil {
		log.Fatal("Failed to create database tables:", err)
	}

	bot, err = discordgo.New("Bot " + *token)
	if err != nil {
		log.Fatal("Error creating Discord session: ", err)
	}

	bot.AddHandler(messageCreate)

	err = bot.Open()
	if err != nil {
		log.Fatal("Error opening Discord connection: ", err)
	}
	defer bot.Close()

	httpServer()
}

func createTables() error {
	_, err := db.Exec(`
      CREATE TABLE IF NOT EXISTS users(
        playername varchar PRIMARY KEY,
        discordid varchar UNIQUE
      )`)
	if err != nil {
		return err
	}
	_, err = db.Exec(`
      CREATE TABLE IF NOT EXISTS games(
        gamename varchar PRIMARY KEY,
        channelid varchar UNIQUE
      )`)
	if err != nil {
		return err
	}
	return nil
}

// Wrapper to log the errors
func sendDiscordReply(s *discordgo.Session, m *discordgo.MessageCreate, msg string) {
	_, err := s.ChannelMessageSend(m.ChannelID, msg)
	if err != nil {
		log.Print("Failed to send discord message:", err)
	}
}

// Discord Stuff
func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.ID == s.State.User.ID {
		return
	}

	var param = strings.SplitN(m.Content, " ", 2)
	if !strings.HasPrefix(param[0], commandPrefix) {
		return
	}

	c := m.ChannelID
	u := m.Author.ID

	switch param[0][len(commandPrefix):] {
	case "setname":
		if len(param) <= 1 {
			sendDiscordReply(s, m, "`setname` requires at least one argument.")
			return
		}

		playerName := param[1]
		err := updatePlayer(u, playerName)
		if err != nil {
			if err == nameTakenError {
				sendDiscordReply(s, m, err.Error())
			} else {
				sendDiscordReply(s, m, "Oops, something went wrong.")
			}
			log.Print(err)
			return
		}
		sendDiscordReply(s, m, "Your player name has been updated.")
	case "setgamechannel":
		if len(param) <= 1 {
			sendDiscordReply(s, m, "`setgamechannel` requires at least one argument.")
			return
		}

		gameName := param[1]
		err := updateGame(c, gameName)
		if err != nil {
			sendDiscordReply(s, m, "Oops, something went awry.")
			log.Print(err)
			return
		}
		sendDiscordReply(s, m, "The game has been added to this channel.")
	case "help":
		sendDiscordReply(s, m, "Commands: setname, setgamechannel")
	default:
		return
	}
}

func sendMessage(res WebhookMessage) error {
	message, err := generateMessage(res)
	if err != nil {
		return err
	}
	gameChannelID, err := getChannelIDFromGame(res.GameName)
	if err != nil {
		return err
	}

	if gameChannelID != "" {
		bot.ChannelMessageSend(gameChannelID, message)
	} else if *defaultChannelID != "" {
		bot.ChannelMessageSend(*defaultChannelID, message)
	} else {
		log.Print("No discord channel to send the turn notification to:", message)
		return nil
	}
	gameTurnCounter[res.GameName], _ = strconv.Atoi(res.TurnNumber)
	return nil
}

func generateMessage(res WebhookMessage) (string, error) {
	discordID, err := getDiscordIDFromUser(res.PlayerName)
	if err != nil {
		return "", err
	}
	if discordID == "" {
		return fmt.Sprintf("Hey %s, it is your turn in game '%s' on turn number %s. To set up discord pings send: %ssetname %s",
			res.PlayerName, res.GameName, res.TurnNumber, commandPrefix, res.PlayerName), nil
	} else {
		return fmt.Sprintf("Hey <@%s>, it is your turn in game '%s' on turn number %s.",
			discordID, res.GameName, res.TurnNumber), nil
	}
}

// Player Stuff

func getDiscordIDFromUser(u string) (string, error) {
	sqlStatement := `SELECT playername, discordid FROM users WHERE playername=$1;`

	var playerName string
	var discordID string

	row := db.QueryRow(sqlStatement, u)
	switch err := row.Scan(&playerName, &discordID); err {
	case sql.ErrNoRows:
		return "", nil
	case nil:
		return discordID, nil
	default:
		return "", err
	}
}

func updatePlayer(u string, playerName string) error {
	id, err := getDiscordIDFromUser(playerName)
	if err != nil {
		return err
	}
	if id != "" {
		return nameTakenError
	}
	_, err = db.Exec("INSERT OR REPLACE INTO users(playername, discordid) VALUES($1, $2)", playerName, u)
	return err
}

// Game Stuff

func getChannelIDFromGame(c string) (string, error) {
	sqlStatement := `SELECT gamename, channelid FROM games WHERE gamename=$1;`

	var gameName string
	var channelID string

	row := db.QueryRow(sqlStatement, c)
	switch err := row.Scan(&gameName, &channelID); err {
	case sql.ErrNoRows:
		return "", nil
	case nil:
		return channelID, nil
	default:
		return "", err
	}
}

func updateGame(c string, gameName string) error {
	_, err := db.Exec("INSERT OR REPLACE INTO games(gamename, channelid) VALUES($1, $2)", gameName, c)
	return err
}

// HTTP Stuff

func httpServer() {
	log.Println("Server is starting...")
	listenAddress := ":8080"
	if os.Getenv("HTTP_PORT") != "" {
		listenAddress = ":" + os.Getenv("HTTP_PORT")
	}
	mux := http.NewServeMux()
	mux.HandleFunc("/webhook", webhook)

	log.Printf("listening on: %v", listenAddress)
	log.Fatal(http.ListenAndServe(listenAddress, requestLogger(mux)))
}

func webhook(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	if r.Method != "POST" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		_, err := io.WriteString(w, "Only POST requests are supported.")
		if err != nil {
			log.Print("Failed to send HTTP response:", err)
		}
	}

	decoder := json.NewDecoder(r.Body)

	var res WebhookMessage

	if err := decoder.Decode(&res); err != nil {
		log.Print("Failed to decode json:", err)
		return
	}

	w.WriteHeader(http.StatusOK)
	_, err := io.WriteString(w, "OK")
	if err != nil {
		log.Print("Failed to send HTTP response:", err)
	}
	turnNumber, _ := strconv.Atoi(res.TurnNumber)
	if gameTurnCounter[res.GameName] == turnNumber {
		log.Print("Ignoring duplicate turn number")
		return
	}

	if err := sendMessage(res); err != nil {
		log.Print("Failed to send notification:", err)
		return
	}
}

func requestLogger(targetMux http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		targetMux.ServeHTTP(w, r)

		// log request by who(IP address)
		requesterIP := r.RemoteAddr

		log.Printf(
			"%s\t\t%s\t\t%s\t\t%v",
			r.Method,
			r.RequestURI,
			requesterIP,
			time.Since(start),
		)
	})
}
