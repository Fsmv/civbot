# CivBot

This bot listens for a webhook from Civilization VI to alert when it is someone's turn.
[This reddit post](https://www.reddit.com/r/civ/comments/aqwwui/play_by_cloud_making_webhook_work_with_discord/)
was very influential in building this. It allows users to set their player name and tie it to their Discord name
so the bot can ping them when it is their turn. If the user has not tied their playername to ID, it will simply
output the playername from the webhook call. From my experience the Civ app will not send a webhook to a URL
with a port number in it (i.e. `http://something.com:8080/webhook`). Additionally, the Mac client does not
seem to send webhooks. I've reported this to 2K but have heard nothing back yet.

## Fork

This is a fork of https://gitlab.com/ahanselka/civbot. Mainly I wanted to use
SQLite instead of Postgres because I didn't want to install a whole database for
a tiny game session with my friends.

I also made the command prefix configurable and cleaned up the error handling so
it won't crash any time anything happens.

## Discord Bot

In order to connect the bot to Discord, you'll need to create a bot on the
[Discord applications page](https://discordapp.com/developers/applications).
Once you create the application, you'll need to create a [bot
user](https://discordapp.com/developers/docs/topics/oauth2#bots). You'll also
need to [add the bot to your
server](https://discordapp.com/developers/docs/topics/oauth2#bot-authorization-flow).
The token for the bot user is what you will set as `--discord_token`.

## Config

You can configure this program with commandline flags or environment variables:

- `--discord_token`, `DISCORD_TOKEN` - The bot user API token from the Discord developer console.
- `--default_discord_channel_id`, `DISCORD_CHANNEL_ID` - The channel ID of the channel the bot should be listening in and sending notices to.
- `--dbfile` - The location of the sqlite database that stores the discord channels to post to and the discord names of the civ players

The following environment variables are optional.

- `HTTP_PORT` - Which port to listen on. The listen IP is currently not configurable.
