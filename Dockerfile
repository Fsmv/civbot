FROM golang:alpine
ADD . /go/src/civbot
RUN go install civbot

FROM alpine:latest
RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*
COPY --from=0 /go/bin/civbot .
ENV PORT 8080
CMD ["./civbot"]
